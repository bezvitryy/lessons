<?php 
    $users = [];
    $users["5"] = ["name" => "Test1", "email" => "Test1@test.com"];
    $users["3"] = ["name" => "Test2", "email" => "Test2@test.com"];
    $users["2"] = ["name" => "Test3", "email" => "Test3@test.com"];
    $users["7"] = ["name" => "Test4", "email" => "Test4@test.com"];
    $users["4"] = ["name" => "Test5", "email" => "Test5@test.com"];
    $users["10"] = ["name" => "Test6", "email" => "Test6@test.com"];
    $users["0"] = ["name" => "Test7", "email" => "Test7@test.com"];
    /*echo "Начальный массив";
    echo "<pre>";
        print_r($users);
    echo "</pre>";
    */
    echo "Количество пользователей сайта = ".count($users)."<br/>";
    echo "Отсотированый по убыванию ID";
    $new = krsort($users);
    echo "<pre>";
        print_r($users);
    echo "</pre>";
    echo "Текущий элемент массива (с максимальным ИД т.к отсортирован по убыванию) = ".current($users)["name"]."<br/>";
    echo "Последний элемент массива (с минимальным ИД т.к отсортирован по убыванию) = ".end($users)["name"]."<br/>";
    echo "Следующий за минимальным ИД т.к отсортирован по убыванию = ".prev($users)["name"]."<br/>";
    reset($users);
    echo "Предыдущий максимальному ИД т.к отсортирован по убыванию = ".next($users)["name"]."<br/>";
    end($users);
    //$key = key($users);
    unset($users[key($users)]);
    echo "Удалил элемент с минимальным ИД";
    echo "<pre>";
        print_r($users);
    echo "</pre>";
?>