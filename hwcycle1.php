<?php
    $users = [];
    $users["5"] = ["name" => "Test1", "email" => "Test1@test.com", "lang" => "ua"];
    $users["3"] = ["name" => "Test2", "email" => "Test2@test.com", "lang" => "en"];
    $users["2"] = ["name" => "Test3", "email" => "Test3@test.com", "lang" => "ua"];
    $users["7"] = ["name" => "Test4", "email" => "Test4@test.com", "lang" => "fr"];
    $users["4"] = ["name" => "Test5", "email" => "Test5@test.com", "lang" => "de"];
    $users["10"] = ["name" => "Test6", "email" => "Test6@test.com", "lang" => "ru"];
    $users["0"] = ["name" => "Test7", "email" => "Test7@test.com", "lang" => "en"];
    $users["1"] = ["name" => "Test2", "email" => "Test2@test.com", "lang" => "ru"];
    $users["9"] = ["name" => "Test2", "email" => "Test2@test.com", "lang" => "ua"];
    $users["6"] = ["name" => "Test1", "email" => "Test1@test.com", "lang" => "en"];
    /*
     - выведите на экран имена пользователей, которые встречаются более одного раза и количество повторений имени;
    */
    $names = [];
    foreach ($users as $user) {
        if (empty($names[$user["name"]])) {
            $names[$user["name"]] = 1;
        } else {
            $names[$user["name"]] +=1;
        }
        
    }
    foreach ($names as $key => $value) {
        if ($value > 1) {
            echo $key." - ".$value."<br/>";
        }
    }
    
    /*
       - разделите пользователей на массивы по языку, каждый массив будет содержать пользователей с одинаковым языком;
    */
    $lang = [];
    foreach($users as $user){
        //$key = array_keys($users,$user);
        //$key = array_search($user, $users);
        if (empty($lang[$user["lang"]])) {
            $lang[$user["lang"]] = [];
        }
        $lang[$user["lang"]][] = $user;
    } 
    echo"<pre>";
        print_r($lang);   
    echo"</pre>";
    /*
      - с помощью цикла сформируйте новый массив, содержащий пользователей в обратном порядке от исходного массива (первый пользователь станет последним, второй — предпоследним и т.д.)
    */
    $users2 = [];
    end($users);
    while ( !empty( current($users) ) ) {
        $users2 = current($users);
        prev($users);
    }
    echo"<pre>";
        print_r($users);   
    echo"</pre>";
    /*
    $usersNew = [];
    foreach ($users as $user) {
        array_unshift($usersNew, $user);
    }
    print_r($usersNew);
    */
?>