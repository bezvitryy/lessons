<?php
    $food = array('fruits' => array('orange', 'banana', 'apple'),
                  'veggie' => array('carrot', 'collard', 'pea'));

    // рекурсивный count
    echo count($food, COUNT_RECURSIVE);  // output 8

    // обычный count
    echo count($food);                  // output 2
    echo "<pre>";
        print_r($food);
    echo "</pre>";

?>